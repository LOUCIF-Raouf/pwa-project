const functions = require("firebase-functions");
const algoliasearch = require("algoliasearch");
const admin = require("firebase-admin");
admin.initializeApp();
const env = functions.config();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });

const client = algoliasearch(env.algolia.appid, env.algolia.apikey);
const index = client.initIndex("restaurants_search");

exports.helloWorld = functions
  .region("europe-west1")
  .https.onRequest((request, response) => {
    response.send("Hello, ESGI");
  });

exports.indexUser = functions
  .region("europe-west1")
  .firestore.document("users/{userId}")
  .onCreate((snap, context) => {
    const data = snap.data();

    data.objectID = context.params.userId;

    if (data.type === "restaurant") return index.saveObject(data);
    else return "";
  });

exports.updateUser = functions
  .region("europe-west1")
  .firestore.document("users/{userId}")
  .onUpdate((change) => {
    const data = change.after.data();
    data.objectID = change.after.id;
    console.log(data);
    console.log(change.after.id);
    if (data.type === "restaurant") return index.saveObject(data);
    else return "";
  });

exports.unindexUser = functions
  .region("europe-west1")
  .firestore.document("users/{userId}")
  .onDelete((snap, context) => {
    console.log(snap);
    const objectId = snap.id;

    return index.deleteObject(objectId);
  });
