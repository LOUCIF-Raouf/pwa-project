importScripts("https://www.gstatic.com/firebasejs/5.9.4/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/5.9.4/firebase-messaging.js");

const firebaseConfig = {
  apiKey: "AIzaSyDGt8fwo3JgY8R8ygaySzGvZoW5HhJ1dfM",
  authDomain: "open-eat-d67d9.firebaseapp.com",
  databaseURL: "https://open-eat-d67d9.firebaseio.com",
  projectId: "open-eat-d67d9",
  storageBucket: "open-eat-d67d9.appspot.com",
  messagingSenderId: "980661141811",
  appId: "1:980661141811:web:ddc0b040a61f1bc1a43e46",
  measurementId: "G-7FNX3538L3",
};

firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function (payload) {
  console.log(payload);
  const promiseChain = clients
    .matchAll({
      type: "window",
      includeUncontrolled: true,
    })
    .then((windowClients) => {
      for (let i = 0; i < windowClients.length; i++) {
        const windowClient = windowClients[i];
        windowClient.postMessage(payload);
      }
    })
    .then(() => {
      return registration.showNotification(payload.data.title, {
        body: payload.data.body,
      });
    });
  return promiseChain;
});
self.addEventListener("notificationclick", function (event) {
  console.log(event);
});
