import React, { useState, useEffect, useContext } from "react";
import Loading from "../Loading/Loading";
import MessageBox from "./MessageBox";
import "./Chat.css";
import AppContext from "../../Context/AppContext";

const ListUsers = ({ listUsers, currentRecipient, setCurrentRecipient }) => {
  const { firebase } = useContext(AppContext);

  return (
    <div>
      {listUsers
        .filter((user) => user.id !== firebase.currentUserUid())
        .map((user, index) => (
          <button
            key={index}
            className={
              currentRecipient && currentRecipient.id === user.id
                ? "ItemFocused"
                : "ItemNotFocused"
            }
            onClick={() => {
              setCurrentRecipient(user);
            }}
          >
            <img
              className="viewAvatarItem"
              src={user.image}
              alt="icon avatar"
            />
            <div className="viewContentItem">
              <span className="textItem">{`${user.firstname} ${user.lastname} `}</span>
            </div>
          </button>
        ))}
    </div>
  );
};

const Chat = () => {
  const [listUsers, setListUsers] = useState([]);
  const [currentRecipient, setCurrentRecipient] = useState(null);
  const [loading, setLoading] = useState(false);
  const { firebase } = useContext(AppContext);

  useEffect(() => {
    setLoading(true);
    firebase
      .getUsers()
      .then((response) => {
        setListUsers(response);
        setLoading(false);
      })
      .catch((error) => console.log(error));
  }, []);

  return (
    <div>
      {loading && <Loading />}
      {!loading && (
        <div className="root">
          <div className="body">
            <div className="viewListUser">
              <ListUsers
                listUsers={listUsers}
                currentRecipient={currentRecipient}
                setCurrentRecipient={setCurrentRecipient}
              />
            </div>
            <div className="viewMessage">
              {currentRecipient ? (
                <MessageBox
                  firebase={firebase}
                  currentRecipient={currentRecipient}
                />
              ) : (
                <></>
              )}
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default Chat;
