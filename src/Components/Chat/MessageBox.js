import { SendOutlined } from "@ant-design/icons";
import moment from "moment";
import React, { useEffect, useRef, useState } from "react";
import "./MessageBox.css";

const MessageBox = ({ firebase, currentRecipient }) => {
  const [inputValue, setInputValue] = useState("");
  const [listMessage, setListMessage] = useState([]);
  let lastMessage = useRef(null);

  useEffect(() => {
    getListHistory();
  }, [currentRecipient]);

  const getListHistory = (reset) => {
    var groupe = "";
    if (
      hashString(firebase.currentUserUid()) <= hashString(currentRecipient.id)
    ) {
      groupe = `${firebase.currentUserUid()}-${currentRecipient.id}`;
    } else {
      groupe = `${currentRecipient.id}-${firebase.currentUserUid()}`;
    }

    firebase
      .getFirestore()
      .collection("messages")
      .doc(groupe)
      .collection(groupe)
      .onSnapshot((snapshot) => {
        let messages = snapshot.docChanges().map((change) => {
          console.log(change);
          return change.doc.data();
        });
        console.log("AVANT", messages);
        messages = messages.filter(
          (message) => !listMessage.includes(message.timestamp)
        );
        console.log("APRES FILTRES", messages);
        setListMessage(messages);
        scrollToBottom();
      });
  };

  const onSendMessage = (content) => {
    if (content.trim() === "") {
      return;
    }
    const timestamp = moment().valueOf().toString();
    var groupe = "";
    if (
      hashString(firebase.currentUserUid()) <= hashString(currentRecipient.id)
    ) {
      groupe = `${firebase.currentUserUid()}-${currentRecipient.id}`;
    } else {
      groupe = `${currentRecipient.id}-${firebase.currentUserUid()}`;
    }
    // setGroupeChatId(groupe);

    const message = {
      idFrom: firebase.currentUserUid(),
      idTo: currentRecipient.id,
      timestamp: timestamp,
      content: content.trim(),
    };

    firebase
      .sendMessage(message, timestamp, groupe)
      .then(() => {
        setInputValue("");
        getListHistory(true);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const renderListMessage = () => {
    if (listMessage.length > 0) {
      let viewListMessage = [];
      listMessage.forEach((item, index) => {
        if (item.idFrom === firebase.currentUserUid()) {
          viewListMessage.push(
            <div className="viewItemRight" key={item.timestamp}>
              <span className="textContentItem">{item.content}</span>
            </div>
          );
        } else {
          viewListMessage.push(
            <div className="viewWrapItemLeft" key={item.timestamp}>
              <div className="viewWrapItemLeft3">
                {isLastMessageLeft(index) ? (
                  <img
                    className="peerAvatarLeft"
                    src={currentRecipient.image}
                    alt="icon avatar"
                  />
                ) : (
                  <div className="viewPaddingLeft" />
                )}
                <div className="viewItemLeft">
                  <span className="textContentItem">{item.content}</span>
                </div>
              </div>
              {isLastMessageLeft(index) ? (
                <span className="textTimeLeft">
                  {moment(Number(item.timestamp)).format("fr")}
                </span>
              ) : null}
            </div>
          );
        }
      });
      return viewListMessage;
    } else {
      return <div></div>;
    }
  };

  function isLastMessageLeft(index) {
    if (
      (index + 1 < listMessage.length &&
        listMessage[index + 1].idFrom === firebase.currentUserUid()) ||
      index === listMessage.length - 1
    ) {
      return true;
    } else {
      return false;
    }
  }

  function isLastMessageRight(index) {
    if (
      (index + 1 < listMessage.length &&
        listMessage[index + 1].idFrom !== firebase.currentUserUid()) ||
      index === listMessage.length - 1
    ) {
      return true;
    } else {
      return false;
    }
  }

  const hashString = (str) => {
    let hash = 0;
    for (let i = 0; i < str.length; i++) {
      hash += Math.pow(str.charCodeAt(i) * 31, str.length - i);
      hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
  };

  const onKeyboardPress = (event) => {
    if (event.key === "Enter") {
      onSendMessage(inputValue);
    }
  };

  const scrollToBottom = () => {
    if (lastMessage)
      if (lastMessage.current) lastMessage.current.scrollIntoView({});
  };

  return (
    <div className="viewChatBoard">
      {/* Header */}
      <div className="headerChatBoard">
        <img
          className="viewAvatarItem"
          src={currentRecipient.image}
          alt="icon avatar"
        />
        <span className="textHeaderChatBoard">
          {currentRecipient.firstname} {currentRecipient.lastname}
        </span>
      </div>

      {/* List message */}
      <div className="viewListContentChat">
        {renderListMessage()}
        <div style={{ float: "left", clear: "both" }} ref={lastMessage} />
      </div>

      {/* View bottom */}
      <div className="viewBottom">
        <input
          className="viewInput"
          placeholder="Type your message..."
          value={inputValue}
          onChange={(e) => setInputValue(e.target.value)}
          onKeyPress={onKeyboardPress}
        />
        <button className="icSend" onClick={() => onSendMessage(inputValue)}>
          <SendOutlined />
        </button>
      </div>
    </div>
  );
};

export default MessageBox;
