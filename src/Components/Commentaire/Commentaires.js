import { StarFilled } from "@ant-design/icons";
import { Button, Comment, Form, Input, List, Rate } from "antd";
import moment from "moment";
import React, { useContext, useEffect, useState } from "react";
import AppContext from "../../Context/AppContext";
import { Loading } from "../Loading/Loading";
import "./Commentaires.css";

const { TextArea } = Input;

const CommentList = ({ comments }) => (
  <List
    dataSource={comments}
    header={`${comments.length} ${comments.length > 1 ? "replies" : "reply"}`}
    itemLayout="horizontal"
    renderItem={(props) => <Commentaire {...props} />}
  />
);

const Commentaire = ({ ...props }) => {
  return (
    <div>
      <div>
        <Rate disabled defaultValue={props.note} />
      </div>
      <div>
        <Comment {...props} />
      </div>
    </div>
  );
};

const CommentForm = ({ form, onFinish }) => (
  <>
    <Form
      form={form}
      name="register"
      onFinish={onFinish}
      scrollToFirstError
      initialValues={
        ({ name: ["note"], value: 0 }, { name: ["comment"], value: "" })
      }
    >
      <Form.Item
        name="note"
        rules={[
          {
            required: true,
            message: "Veuillez choisir une note!",
          },
        ]}
      >
        <Rate character={<StarFilled />} />
      </Form.Item>
      <Form.Item
        name="comment"
        rules={[
          {
            required: true,
            message: "Veuillez saisir un commentaire!",
          },
        ]}
      >
        <TextArea rows={4} />
      </Form.Item>
      <Form.Item>
        <Button htmlType="submit" type="primary">
          Add Comment
        </Button>
      </Form.Item>
    </Form>
  </>
);

const Commentaires = ({ dataRestaurant }) => {
  const [comments, setComments] = useState([]);
  const [loading, setLoading] = useState(false);
  const [form] = Form.useForm();
  const {
    isDisconnected,
    getLocalComments,
    localComments,
    setLocalComments,
    getLocalUser,
    localUser,
    firebase,
  } = useContext(AppContext);

  useEffect(() => {
    getLocalComments();
    getLocalUser();
  }, []);

  const onFinish = (values) => {
    // Add a new document with a generated id.
    console.log("ok", localUser);
    console.log({ values, dataRestaurant });

    if (isDisconnected) {
      let comment = {
        author: localUser.displayName
          ? localUser.displayName
          : localUser.firstname + " " + localUser.lastname,
        comment: values.comment,
        note: values.note,
        uuid_restaurant: dataRestaurant,
        uuid_user: localUser.id,
        datetime: moment().format("DD/MM/YYYY HH:mm"),
        sync: "add",
      };

      localComments.push(comment);
      setLocalComments(localComments);
      form.resetFields();

      console.log(localComments);
    } else {
      firebase
        .setComment(values, dataRestaurant)
        .then((response) => {
          setComments(response);
          form.resetFields();
        })
        .catch((error) => console.log(error));
    }
  };
  useEffect(() => {
    setLoading(true);

    if (isDisconnected) {
      if (localComments) {
        setComments(
          localComments.filter((c) => c.uuid_restaurant === dataRestaurant)
        );
      }
      setLoading(false);
    } else {
      firebase
        .getComments(dataRestaurant)
        .then((response) => {
          setComments(response);
          setLoading(false);
        })
        .catch((error) => console.log(error));
    }
  }, []);

  console.log(localComments);
  return (
    <div>
      <>
        <Comment content={<CommentForm onFinish={onFinish} form={form} />} />
      </>
      {loading && <Loading />}
      {!loading && comments && (
        <CommentList
          comments={
            isDisconnected
              ? localComments
                ? localComments
                    .filter((c) => c.uuid_restaurant === dataRestaurant)
                    .map((co) => {
                      return { ...co, content: co.comment };
                    })
                : []
              : comments
          }
        />
      )}
    </div>
  );
};

export default Commentaires;
