import firebaseApp from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/messaging";
import "firebase/storage";
import moment from "moment";

const pwaConfig = {
  apiKey: process.env.REACT_APP_API_KEY,
  authDomain: process.env.REACT_APP_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_DATABASE_URL,
  projectId: process.env.REACT_APP_PROJECT_ID,
  storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
  appId: process.env.REACT_APP_ID,
  measurementId: process.env.REACT_APP_MEASUREMENT_ID,
};

class Firebase {
  constructor() {
    firebaseApp.initializeApp(pwaConfig);
    this.auth = firebaseApp.auth();
    this.db = firebaseApp.firestore();
    this.storage = firebaseApp.storage();
    this.messaging = firebaseApp.messaging();
  }

  format = (doc) => {
    const data = doc.data();
    const id = doc.id;
    return { id, ...data };
  };

  //connexion

  login = (email, password) =>
    this.auth.signInWithEmailAndPassword(email, password);

  //deconnexion
  signOut = () => this.auth.signOut();

  //inscription
  signUp = (email, password) =>
    this.auth.createUserWithEmailAndPassword(email, password);

  doPasswordReset = (email) => this.auth.sendPasswordResetEmail(email);

  doPasswordUpdate = (password) =>
    this.auth.currentUser.updatePassword(password);

  currentUser = () => this.auth.currentUser;

  currentUserUid = () => {
    if (this.auth.currentUser) {
      return this.auth.currentUser.uid;
    }
    return 0;
  };

  getCurrentUsername() {
    return this.auth.currentUser && this.auth.currentUser.displayName;
  }

  user = (uid) => {
    return this.db.doc(`users/${uid}`);
  };

  getUsers = async () => {
    const snapshot = await this.db
      .collection("users")
      .where("type", "==", "utilisateur")
      .get();
    return snapshot.docs.map((doc) => {
      return this.format(doc);
    });
  };

  getRestaurants = async () => {
    const snapshot = await this.db
      .collection("users")
      .where("type", "==", "restaurant")
      .get();

    return snapshot.docs.map((doc) => {
      return this.format(doc);
    });
  };

  getUser = (id) => {
    return this.db
      .doc(`users/${id}`)
      .get()
      .then((doc) => {
        if (doc.data()) {
          return this.format(doc);
        }
      });
  };

  getComments = async (idRestaurant) => {
    const snapshot = await this.db
      .collection("comment")
      .where("uuid_restaurant", "==", idRestaurant)
      .get();

    return snapshot.docs.map((doc) => {
      return this.format(doc);
    });
  };

  getAllComments = async () => {
    const snapshot = await this.db.collection("comment").get();

    return snapshot.docs.map((doc) => {
      return this.format(doc);
    });
  };

  getOrder = (id) => {
    return this.db
      .doc(`order/${id}`)
      .get()
      .then((doc) => {
        if (doc.data()) {
          return this.format(doc);
        }
      });
  };

  getOrderByUser = async (id) => {
    const snapshot = await this.db
      .collection("order")
      .where("id_utilisateur", "==", id)
      .get();

    return snapshot.docs.map((doc) => {
      return this.format(doc);
    });
  };

  addOrder = (id_utilisateur, id_marchant, commande) => {
    return this.db
      .collection("order")
      .add({
        commande: commande,
        id_utilisateur: id_utilisateur,
        id_marchant: id_marchant,
        status: 'En attente de validation'
      })
      .then(function (docRef) {
        console.log("Document written with ID: ", docRef.id);
        return docRef.id;
      })
      .catch(function (error) {
        console.error("Error adding document: ", error);
      });
  };

  setOrder = async (id, order) => {
    return await this.db.doc(`order/${id}`).set(order);
  };

  sendOrder = async (id) => {
    this.db.doc(`order/${id}`).update({
      date_commande: moment().format("DD/MM/YYYY HH:mm"),
      status: "En cours de préparation"
    });
    return await this.getOrder(id)
  };

  setComment = (values, id_marchant) => {
    console.log(values.comment, id_marchant);
    this.db.collection("comment").add({
      author: values.author ? values.author : this.getCurrentUsername(),
      content: values.comment,
      note: values.note,
      uuid_restaurant: id_marchant,
      uuid_user: this.currentUserUid(),
      datetime: moment().format("DD/MM/YYYY HH:mm"),
    });
    return this.getComments(id_marchant);
  };

  setFavorite = async (idRestaurant) => {
    var userRef = this.db
      .collection("users")
      .doc(this.currentUserUid())
      .get()
      .then((doc) => {
        if (doc.data()) {
          return this.format(doc);
        }
      });
    var val = await userRef.then(function (values) {
      let favoritesRef = [];
      if (values.favorites) {
        favoritesRef = values.favorites;
      }
      if (favoritesRef.includes(idRestaurant)) {
        var index = favoritesRef.indexOf(idRestaurant);
        favoritesRef.splice(index, 1);
      } else {
        favoritesRef.push(idRestaurant);
      }
      return favoritesRef;
    });
    this.db.doc(`users/${this.currentUserUid()}`).update({
      favorites: val,
    });
  };

  async getImage(image) {
    return await this.storage
      .ref(`images`)
      .child(`${image}`)
      .getDownloadURL()
      .then((url) => {
        console.log(url);
        return url;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  async getImageByCurrentUser() {
    return await this.getUser(this.currentUserUid()).then((user) => {
      return user.image;
    });
  }

  async pushImage(image) {
    this.storage.ref(`images/profil-${this.currentUserUid()}`).put(image);
    this.db.doc(`users/${this.currentUserUid()}`).update({
      image: await this.getImage(`profil-${this.currentUserUid()}`),
    });

    return await this.getImage("profil-" + this.currentUserUid());
  }

  async sendMessage(message, time, groupChatId) {
    this.db
      .collection("messages")
      .doc(groupChatId)
      .collection(groupChatId)
      .doc(time)
      .set(message);
  }

  async getMessages(groupChatId) {
    return await this.db
      .collection("messages")
      .doc(groupChatId)
      .collection(groupChatId)
      .get();
  }

  getFirestore = () => this.db;
}

export default Firebase;
