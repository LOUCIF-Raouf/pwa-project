import React, { useContext, useEffect } from "react";
import { NavLink, Redirect, useHistory } from "react-router-dom";
import AppContext from "../../Context/AppContext";
import "./SignForm.css";

export const SignForm = ({ children }) => {
  const { user, firebase } = useContext(AppContext);
  let history = useHistory();

  useEffect(() => {
    if (window.location.pathname === "/auth/logout") {
      firebase.signOut();
      history.push({ pathname: "login" });
    }
  });

  return (
    <div>
      <div className="FormTitle">
        <NavLink
          exact
          to="/auth/login"
          activeClassName="FormTitle__Link--Active"
          className="FormTitle__Link"
        >
          Sign In
        </NavLink>
        or
        <NavLink
          to="/auth/register"
          activeClassName="FormTitle__Link--Active"
          className="FormTitle__Link"
        >
          Sign Up
        </NavLink>
      </div>
      {user ? (
        window.location.pathname !== "/auth/logout" ? (
          <Redirect to="/app" />
        ) : (
          children
        )
      ) : (
        children
      )}
    </div>
  );
};

export default SignForm;
