import React, { useState, useEffect, useContext } from "react";
import { Button } from "antd";
import { useHistory } from "react-router-dom";
import AppContext from "../../Context/AppContext";

const Uploader = () => {
  let history = useHistory();
  const [image, setImage] = useState(null);
  const [url, setUrl] = useState("");
  const [error, setError] = useState("");
  const {firebase } = useContext(AppContext);

  useEffect(() => {
    firebase
    .getImageByCurrentUser()
    .then((response) => {
      setUrl(response);
    })
    .catch((error) => console.log(error));

  }, []);

  const handChange = (e) => {
    const file = e.target.files[0];
    if (file) {
      const fileType = file["type"];
      const validImageTypes = ["image/gif", "image/jpeg", "image/png"];
      if (validImageTypes.includes(fileType)) {
        setError("");
        setImage(file);
      } else {
        setError("Please select an image to upload");
      }
    }
  };

  const handleUpdate = () => {
    if (image) {
      firebase.pushImage(image).then((url) => {
        setUrl(url);
      });
    } else {
      setError("Error please choose an image to upload");
    }
  };

  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        flexDirection: "column",
        justifyContent: "center",
        overflow: "auto",
      }}
    >
      <div>
        <input type="file" onChange={handChange} />{" "}
        <Button onClick={handleUpdate} type="primary">
          {" "}
          Upload
        </Button>
      </div>
      <div style={{ height: "100px" }}>
        <p style={{ color: "red" }}>{error}</p>
      </div>
      <img src={url} alt="Profil Picture" width="300px" height="300px" />
      <hr />
      <Button type="primary" htmlType="submit" onClick={profil}>
        Return Profil
      </Button>
    </div>
  );

  async function profil() {
    history.replace("/app/user/" + firebase.currentUserUid());
  }
};

export default Uploader;
