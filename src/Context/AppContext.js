import jwt from "jwt-decode";
import localforage from "localforage";
import React, { createContext, useEffect, useReducer, useState } from "react";
import { useHistory } from "react-router-dom";
import { withFirebase } from "../Components/Firebase";
import Loading from "../Components/Loading/Loading";

const initialState = {
  test: "sami",
};

export const AppContext = createContext();

const reducer = (state, actions) => {
  switch (actions.type) {
    case "SET_TOKEN":
      return { ...state, token: actions.payload };
    case "SET_USER":
      return { ...state, user: actions.payload };
    case "SET_IS_DISCONNECTED":
      return { ...state, user: actions.payload };
    case "SET_RESTAURANTS":
      return { ...state, rests: actions.payload, searchRest: actions.payload };
    case "SET_LOCAL_COMMENTS":
      localforage.setItem("commentaires", actions.payload);
      return { ...state, localComments: actions.payload };
    case "SET_LOCAL_USER":
      localforage.setItem("current_user", actions.payload);
      return { ...state, localUser: actions.payload };
    case "SET_SEARCH_OFFLINE_REST":
      return { ...state, searchRest: actions.payload };
    default:
      return state;
  }
};

export const AppProvider = ({ children, firebase }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const [loading, setLoading] = useState(false);
  const [isDisconnected, setIsDisconnected] = useState(false);
  let history = useHistory();
  let webPing = null;

  const handleConnectionChange = () => {
    const condition = navigator.onLine ? "online" : "offline";
    if (condition === "online") {
      webPing = setInterval(() => {
        fetch("//google.com", {
          mode: "no-cors",
        })
          .then(() => {
            setIsDisconnected(false);
          })
          .catch(() => setIsDisconnected(true));
      }, 2000);
      return;
    }

    return setIsDisconnected(true);
  };

  const sync = async () => {
    console.log("ok sync");
    if (isDisconnected) {
      console.log("offline", isDisconnected);
    } else {
      setLoading(true);
      // GET data of localforage
      let x = await syncIndexedDBToFirebase();
      let y = await syncFirebaseToIndexedDB();

      console.log(x);
      console.log(y);
      console.log("onlin", isDisconnected);
      setLoading(false);
    }
  };

  const syncUser = () => {
    localforage.getItem("current_user").then((user) => {
      if (user) {
        user.sync = "sync";
        localforage.setItem("current_user", user);

        let id = user.id;
        delete user.refreshToken;
        delete user.emailVerified;
        delete user.displayName;
        delete user.id;
        delete user.token;

        firebase.user(id).set(user);
      }
    });
  };

  const syncRestaurants = () => {
    localforage.getItem("restaurants").then((restaurant) => {
      if (restaurant) {
        let localR = restaurant.map((r) => {
          if (r.sync) {
            if (r.sync === "update") {
              return { ...r, sync: "sync" };
            } else return { ...r };
          } else return { ...r };
        });
        localforage.setItem("restaurants", localR);

        let restaurantToUpdate = restaurant
          .filter((re) => {
            if (re.sync) {
              return re.sync === "update";
            }
          })
          .map((r) => {
            return { ...r, sync: "sync" };
          });

        if (restaurantToUpdate) {
          restaurantToUpdate.map((r) => {
            delete r.id;
            firebase.getRestaurants().then((response) => {
              response.map((r) => {
                response.set(restaurantToUpdate);
              });
            });
          });
        }
      }
    });
  };

  const syncComments = () => {
    localforage.getItem("commentaires").then((commentaire) => {
      if (commentaire) {
        let localC = commentaire.map((c) => {
          if (c.sync) {
            if (c.sync === "add") {
              return { ...c, sync: "sync" };
            } else return { ...c };
          } else return { ...c };
        });
        localforage.setItem("commentaires", localC);

        let commentaireToAdd = commentaire
          .filter((co) => {
            if (co.sync) {
              return co.sync === "add";
            }
          })
          .map((c) => {
            return { ...c, sync: "sync" };
          });

        if (commentaireToAdd) {
          commentaireToAdd.map((r) => {
            delete r.id;
            firebase.setComment(r, r.uuid_restaurant);
          });
        }
      }
    });
  };

  const syncIndexedDBToFirebase = () => {
    return new Promise((resolve) => {
      console.log("SYNC DATA");
      syncUser();
      syncRestaurants();
      syncComments();
      resolve("resolved");
    });
  };

  const syncFirebaseToIndexedDB = () => {
    return new Promise((resolve) => {
      firebase.getRestaurants().then(async (response) => {
        localforage.setItem("restaurants", response);
      });
      firebase
        .getAllComments()
        .then((response) => localforage.setItem("commentaires", response));
      console.log("SYNC DATA f to db");
      resolve("fini");
    });
  };

  useEffect(() => {
    sync();
    console.log("on a finis sync useffect");
    clearInterval(webPing);
  }, [isDisconnected]);

  useEffect(() => {
    setLoading(true);

    handleConnectionChange();
    window.addEventListener("online", handleConnectionChange);
    window.addEventListener("offline", handleConnectionChange);

    firebase.auth.onAuthStateChanged((user) => {
      if (user) {
        firebase
          .getUser(user.uid)
          .then(async (response) => {
            let currentUser = {
              id: user.uid,
              displayName: user.displayName
                ? user.displayName
                : response.firstname + " " + response.lastname,
              emailVerified: user.emailVerified,
              refreshToken: user.refreshToken,
              token: user.xa,
              address: response.address,
              email: response.email,
              firstname: response.firstname,
              lastname: response.lastname,
              phone: response.phone,
              type: response.type,
              image: response.image,
              favorites: response.favorites,
            };

            value.setUser(currentUser);
            console.log(currentUser);
            value.setLocalUser(currentUser);
          })
          .catch((error) => console.error(error));
      } else {
        console.log(user, "no signed in");
        history.push("/auth/login");
      }
      setLoading(false);
    });
  }, []);

  const value = {
    state: state,
    token: state.token,
    firebase: firebase,
    decodedToken: state.token ? jwt(state.token) : false,
    setToken: (token) => {
      dispatch({ type: "SET_TOKEN", payload: token });
    },
    setUser: (user) => {
      dispatch({ type: "SET_USER", payload: user });
    },
    isDisconnected: isDisconnected,
    restaurants: () =>
      localforage.getItem("restaurants").then((r) => {
        dispatch({ type: "SET_RESTAURANTS", payload: r });
      }),
    rests: state.rests,
    getLocalComments: () =>
      localforage.getItem("commentaires").then((r) => {
        dispatch({ type: "SET_LOCAL_COMMENTS", payload: r });
      }),
    setLocalComments: (comments) =>
      dispatch({ type: "SET_LOCAL_COMMENTS", payload: comments }),
    localComments: state.localComments,
    getLocalUser: () =>
      localforage.getItem("current_user").then((r) => {
        dispatch({ type: "SET_LOCAL_USER", payload: r });
      }),
    setLocalUser: (user) => {
      dispatch({ type: "SET_LOCAL_USER", payload: user });
    },
    localUser: state.localUser,
    // setLocalFavoritesUser: (favorites) => dispatch({type: "SET_LOCAL_FAVORITES_USER", payload: favorites}),
    // getLocalFavoritesUser: () => localforage.getItem("current_user").then(r => { dispatch({type: "SET_"})})
    user: state.user,
    searchOffline: (e) => {
      var updatedList = state.rests;

      console.log(state.rests);
      updatedList = updatedList.filter(function (item) {
        return (
          item.name.toLowerCase().search(e.target.value.toLowerCase()) !== -1
        );
      });
      dispatch({ type: "SET_SEARCH_OFFLINE_REST", payload: updatedList });
    },
    searchRest: state.searchRest,
  };

  return (
    <AppContext.Provider value={value}>
      {loading ? <Loading /> : children}
    </AppContext.Provider>
  );
};

export const AppProviderFirebase = withFirebase(AppProvider);

export default AppContext;
