import algoliasearch from "algoliasearch/lite";
import React, { useContext, useEffect, useState } from "react";
import { Hits, InstantSearch, SearchBox } from "react-instantsearch-dom";
import AppContext from "../../Context/AppContext";
import { Restaurant } from "../Restaurants/Restaurants";
import styles from "./Home.module.scss";

const searchClient = algoliasearch(
  "INR83I0AFB",
  "af2b77963064c59804822f21e7029cb8"
);

const Hit = ({ hit }) => {
  return (
    <div className={styles.hit}>
      <Restaurant restaurant={hit} />
    </div>
  );
};

const Home = () => {
  const {
    isDisconnected,
    restaurants,
    rests,
    searchOffline,
    searchRest,
  } = useContext(AppContext);
  const [query, setQuery] = useState("");
  // const [initialRestaurants, setInitialRestaurants] = useState(rests);

  useEffect(() => {
    restaurants();
  }, []);

  const search = (e) => {
    setQuery(e.target.value);

    // setInitialRestaurants(updatedList);
  };

  return (
    <div>
      {!isDisconnected ? (
        <InstantSearch
          indexName="restaurants_search"
          searchClient={searchClient}
        >
          <SearchBox className={styles.searchBox} />
          <Hits className={styles.hits} hitComponent={Hit} />
        </InstantSearch>
      ) : (
        <div>
          <div className={styles.searchBox}>
            <input
              value={query}
              onChange={(e) => {
                search(e);
                searchOffline(e);
              }}
            />
          </div>
          {searchRest && (
            <div className={styles.restaurants}>
              {searchRest.map((r) => (
                <Restaurant restaurant={r} />
              ))}
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default Home;
