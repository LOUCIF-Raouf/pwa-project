import {
  CalendarOutlined,
  FieldTimeOutlined,
  LoginOutlined,
  LogoutOutlined,
  UserOutlined
} from "@ant-design/icons";
import {
  Alert,
  AutoComplete,
  Button,
  DatePicker,
  Drawer,
  Form,
  Input,
  Layout as Wrapper,
  Menu,
  Modal,
  TimePicker
} from "antd";
import moment from "moment";
import React, { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import AppContext from "../../Context/AppContext";
import styles from "./layout.module.scss";

const { Header, Content, Footer } = Wrapper;
const { SubMenu } = Menu;
const { RangePicker } = TimePicker;

const Logo = () => {
  return (
    <div
      className="logo"
      style={{
        width: "120px",
        height: "31px",
        background: "lightgray",
        margin: "16px 24px 16px 0",
        float: "left",
      }}
    ></div>
  );
};

const NavMenu = ({ setVisible2, vertical }) => {
  const [current, setCurrent] = useState("position");
  const [selectedOption, setSelectedOption] = useState(false);
  const [editDelivery, setEditDelivery] = useState(false);
  const [visible, setVisible] = useState(false);
  const { user, firebase, isDisconnected } = useContext(AppContext);

  const handleClick = (e) => {
    console.log("click ", e);
    setCurrent(e.key);

    if (e.key === "setting:1")
      setSelectedOption({
        icon: <FieldTimeOutlined />,
        content: "Livrer maintenant",
      });
    else if (e.key === "setting:2") {
      setSelectedOption({
        icon: <CalendarOutlined />,
        content: "Planifier pour plus tard",
      });
      setVisible(true);
    }
  };
  const mockVal = (str, repeat = 1) => {
    return {
      value: str.repeat(repeat),
    };
  };
  const onSearch = (searchText) => {
    setEditDelivery(
      !searchText
        ? []
        : [mockVal(searchText), mockVal(searchText, 2), mockVal(searchText, 3)]
    );
  };
  const onSelect = (data) => {
    console.log("onSelect", data);
  };

  const onSearch2 = (searchText) => {
    firebase.searchRestaurants(searchText);
  };

  const onCreate = (values) => {
    console.log("Received values of form: ", values);
    setVisible(false);
    setSelectedOption({
      icon: <CalendarOutlined />,
      content:
        values["date"].format("ddd e MMM") +
        " " +
        values["rangepicker"][0].format("HH:mm") +
        "-" +
        values["rangepicker"][1].format("HH:mm"),
    });
  };

  const inputStyle = {
    border: "none",
    background: "transparent",
    color: "black",
    boxShadow: "unset",
  };

  const menu = [
    {
      type: "menu",
      icon: <LoginOutlined />,
      style: { float: "left" },
      item: {
        key: "login",
        type: "link",
        content: "Commander",
        link: "/app",
      },
    },
    user && {
      type: "submenu",
      icon: <UserOutlined />,
      title: user.displayName,
      titleGroup: "Menu utilisateur",
      style: { float: "right" },
      disabled: isDisconnected,
      items: [
        {
          key: "setting:3",
          icon: <UserOutlined />,
          content: "Compte",
          type: "link",
          to: `/app/user/${user.id}`,
        },
        {
          key: "setting:5",
          icon: <LogoutOutlined />,
          content: "Se déconnecter",
          type: "link",
          to: "/auth/logout",
        },
      ],
    },
    !user && {
      type: "menu",
      icon: <LoginOutlined />,
      style: { float: "right" },
      item: {
        key: "login",
        type: "link",
        content: "Se connecter",
        link: "/auth/login",
      },
    },
  ];

  return (
    <nav className={styles.test}>
      <Menu
        onClick={handleClick}
        selectedKeys={[current]}
        mode={vertical ? "vertical" : "horizontal"}
        selectable={false}
        triggerSubMenuAction="click"
        subMenuCloseDelay={1.5}
      >
        {menu.map((m, index) => {
          if (m) {
            if (m.type === "menu") {
              return (
                <Menu.Item key={index} icon={m.icon} style={m.style}>
                  {m.item.type === "autocomplete" && (
                    <AutoComplete
                      options={m.item.options}
                      style={{ width: 200 }}
                      onSelect={m.item.onSelect}
                      onSearch={m.item.onSearch}
                      placeholder={m.item.placeholder}
                    >
                      <Input style={inputStyle} />
                    </AutoComplete>
                  )}
                  {m.item.type === "link" && (
                    <Link to={m.item.link}>{m.item.content}</Link>
                  )}
                </Menu.Item>
              );
            } else if (m.type === "submenu") {
              return (
                <SubMenu
                  icon={m.icon}
                  title={m.title}
                  key={index}
                  style={m.style}
                  disabled={m.disabled}
                >
                  <Menu.ItemGroup title={m.titleGroup}>
                    {m.items.map((i) => (
                      <Menu.Item key={i.key}>
                        {i.type === "link" ? (
                          <Link to={i.to}>
                            {i.icon} {i.content}
                          </Link>
                        ) : (
                          <>
                            {i.icon} {i.content}
                          </>
                        )}
                      </Menu.Item>
                    ))}
                  </Menu.ItemGroup>
                </SubMenu>
              );
            } else {
              return "";
            }
          }
        })}
        <PlanifyDeliveryForm
          visible={visible}
          onCreate={onCreate}
          onCancel={() => setVisible(false)}
        />
      </Menu>
      <Button
        style={{ float: "right", margin: "16px 0px" }}
        onClick={() => setVisible2(true)}
        type="primary"
      >
        Menu
      </Button>
    </nav>
  );
};
const Navigation = () => {
  const [visible2, setVisible2] = useState(false);

  return (
    <>
      <div className={styles.menu}>
        <NavMenu setVisible2={setVisible2} />
      </div>
      <Drawer
        title="Menu"
        placement={"right"}
        closable={false}
        onClose={() => setVisible2(false)}
        visible={visible2}
        key={"right"}
        width={320}
      >
        <div className={styles.menuMobile}>
          <NavMenu setVisible2={setVisible2} vertical />
        </div>
      </Drawer>
    </>
  );
};

const PlanifyDeliveryForm = ({ visible, onCreate, onCancel }) => {
  const [form] = Form.useForm();

  return (
    <Modal
      visible={visible}
      title="Choisissez l'heure"
      okText="Valider"
      cancelText="Quitter"
      onCancel={onCancel}
      onOk={() => {
        form
          .validateFields()
          .then((values) => {
            console.log(values);
            form.resetFields();
            onCreate(values);
          })
          .catch((info) => {
            console.log("Validate Failed:", info);
          });
      }}
    >
      <Form
        form={form}
        layout="vertical"
        name="form_in_modal"
        initialValues={{
          date: moment(),
        }}
      >
        <Form.Item
          name="date"
          rules={[
            {
              required: true,
              message: "Please input your date of deleviry!",
            },
          ]}
        >
          <DatePicker />
        </Form.Item>
        <Form.Item
          name="rangepicker"
          rules={[
            {
              required: true,
              message: "Please input your date of deleviry!",
            },
          ]}
        >
          <RangePicker format="HH:mm" minuteStep={15} />
        </Form.Item>
      </Form>
    </Modal>
  );
};

const Head = () => {
  return (
    <Header style={{ background: "white" }}>
      <Logo />
      <Navigation />
    </Header>
  );
};

const Foot = () => {
  return <Footer style={{ textAlign: "center" }}>Open Eat ©2020</Footer>;
};

const Layout = ({ children }) => {
  const { firebase, isDisconnected } = useContext(AppContext);

  const test = async () => {
    firebase.messaging
      .requestPermission()
      .then(async function () {
        const token = await firebase.messaging.getToken();
      })
      .catch(function (err) {
        console.log("Unable to get permission to notify.", err);
      });
    navigator.serviceWorker.addEventListener("message", (message) =>
      console.log(message)
    );
  };

  useEffect(() => {
    test();
  }, []);

  return (
    <Wrapper className="layout">
      <Head />
      {isDisconnected && (
        <Alert
          message="Il semblerait que vous ayez perdu votre connexion internet. Vous serez limité dans vos actions."
          type="warning"
          showIcon
          closable
        />
      )}
      <Content
        style={{
          padding: "0 50px",
          height: `${
            isDisconnected
              ? "calc(100vh - 64px - 70px - 40px)"
              : "calc(100vh - 64px - 70px"
          }`,
          overflow: "auto",
        }}
      >
        {children}
      </Content>
      <Foot />
    </Wrapper>
  );
};

export default Layout;
