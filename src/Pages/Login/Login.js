import { LockOutlined, UserOutlined } from "@ant-design/icons";
import { Button, Checkbox, Col, Form, Input, Row } from "antd";
import React, { useContext } from "react";
import { Link, useHistory } from "react-router-dom";
import { withFirebase } from "../../Components/Firebase";
import AppContext from "../../Context/AppContext";

const Login = ({ firebase }) => {
  let history = useHistory();
  const [form] = Form.useForm();
  const { state, token, decodedToken, setToken } = useContext(AppContext);

  const onFinish = (values) => {
    console.log("Success:", values);

    firebase
      .login(values.username, values.password)
      .then((response) => {
        setToken(response.user.xa);
        history.replace("/app/user/" + firebase.currentUserUid());
      })
      .catch((error) => {
        console.log(error);
        switch (error.code) {
          case "auth/wrong-password":
            form.setFields([
              {
                name: "username",
                errors: [error.message],
              },
              {
                name: "password",
                errors: [error.message],
              },
            ]);
            break;
          default:
            console.log("no error ", error);
            break;
        }
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <Row
      style={{
        display: "flex",
        alignItems: "center",
        flexDirection: "column",
        justifyContent: "center",
        overflow: "auto",
      }}
    >
      <Col>
        <Form
          name="normal_login"
          className="login-form"
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          form={form}
        >
          <Form.Item
            name="username"
            rules={[{ required: true, message: "Please input your Username!" }]}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Username"
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[{ required: true, message: "Please input your Password!" }]}
          >
            <Input
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder="Password"
            />
          </Form.Item>
          <Form.Item>
            <Form.Item name="remember" valuePropName="checked" noStyle>
              <Checkbox>Remember me</Checkbox>
            </Form.Item>

            <a className="login-form-forgot" href="/">
              Forgot password
            </a>
          </Form.Item>

          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
              style={{ width: "100%" }}
            >
              Log in
            </Button>
            Or
            <Link to="/auth/sign-up" className="FormField__Link">
              Create an account
            </Link>
          </Form.Item>
        </Form>
      </Col>
    </Row>
  );
};

const SignInForm = withFirebase(Login);

export default SignInForm;
