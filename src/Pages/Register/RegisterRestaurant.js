import {
  Button,
  Checkbox,
  Form,
  Input,
  InputNumber,
  Select,
  TimePicker
} from "antd";
import moment from "moment";
import React, { useContext } from "react";
import AppContext from "../../Context/AppContext";
import types from "./type.mockup.json";

const { Option } = Select;
const format = "HH:mm";

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    xs: {
      span: 240,
    },
    sm: {
      span: 16,
    },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

const RegistrationRestaurantForm = () => {
  const { firebase } = useContext(AppContext);
  const [form] = Form.useForm();

  const onFinish = (values) => {
    console.log("Received values of form: ", values);

    let x = firebase
      .signUp(values.email, values.password)
      .then((response) => {
        return firebase.user(response.user.uid).set({
          name: values.name,
          type_food: values.type,
          email: values.email,
          phone: values.phone,
          address: values.address,
          opening_hour: moment(values.openingHour).format("HH:mm"),
          closing_hour: moment(values.closingHour).format("HH:mm"),
          delivery_fees: values.deliveryPrice,
          type: "restaurant",
        });
      })
      .catch((error) => {
        switch (error.code) {
          case "auth/email-already-in-use":
            form.setFields([
              {
                name: "email",
                errors: [error.message],
              },
            ]);
            break;
          case "auth/weak-password":
            form.setFields([
              {
                name: "password",
                errors: [error.message],
              },
            ]);
            break;
          default:
            console.log("no error ", error);
            break;
        }
      });

      console.log(x);
  };

  const prefixSelector = (
    <Form.Item name="prefix" noStyle>
      <Select
        style={{
          width: 70,
        }}
      >
        <Option value="33">+33</Option>
      </Select>
    </Form.Item>
  );
  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        flexDirection: "column",
        justifyContent: "center",
        height: "100vh",
      }}
    >
      <Form
        {...formItemLayout}
        form={form}
        name="register"
        onFinish={onFinish}
        initialValues={{
          prefix: "33",
        }}
        scrollToFirstError
      >
        <Form.Item
          name="name"
          label="Nom de société"
          rules={[
            {
              required: true,
              message: "Veuillez saisir votre nom de société!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="type"
          label="Type"
          rules={[
            {
              required: true,
              message: "Veuillez selectionner un type!",
            },
          ]}
        >
          <Select defaultValue="default">
            <Option value="default" disabled>
              Selectionner un type
            </Option>
            {types.map((type) => (
              <Option value={type.key}>{type.name}</Option>
            ))}
          </Select>
        </Form.Item>

        <Form.Item
          name="email"
          label="E-mail"
          rules={[
            {
              type: "email",
              message: "L'e-mail n'est pas valide!",
            },
            {
              required: true,
              message: "Veuillez saisir votre E-mail!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="password"
          label="Mot de passe"
          rules={[
            {
              required: true,
              message: "Veuillez saisir votre mot de passe!",
            },
          ]}
          hasFeedback
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          name="confirm"
          label="Confirmer le mot de passe"
          dependencies={["password"]}
          hasFeedback
          rules={[
            {
              required: true,
              message: "Veuillez confirmer votre mot de passe!",
            },
            ({ getFieldValue }) => ({
              validator(rule, value) {
                if (!value || getFieldValue("password") === value) {
                  return Promise.resolve();
                }

                return Promise.reject(
                  "Les deux mots de passe que vous avez saisis ne correspondent pas!"
                );
              },
            }),
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          name="phone"
          label="Phone Number"
          rules={[
            {
              required: true,
              message: "Veuillez entrer votre numéro de téléphone!",
            },
          ]}
        >
          <Input
            addonBefore={prefixSelector}
            style={{
              width: "100%",
            }}
          />
        </Form.Item>

        <Form.Item
          name="address"
          label="Adresse"
          rules={[
            {
              required: true,
              message: "Veuillez entrer votre adresse!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="openingHour"
          label="Heure d'ouverture"
          rules={[
            {
              required: true,
              message: "Veuillez entrer votre heure d'ouverture!",
            },
          ]}
        >
          <TimePicker defaultValue={moment("12:08", format)} format={format} />
        </Form.Item>

        <Form.Item
          name="closingHour"
          label="Heure de fermeture"
          rules={[
            {
              required: true,
              message: "Veuillez entrer votre heure de fermeture!",
            },
          ]}
        >
          <TimePicker defaultValue={moment("12:08", format)} format={format} />
        </Form.Item>

        <Form.Item
          name="deliveryPrice"
          label="Frais de livraison"
          rules={[
            {
              required: true,
              message: "Veuillez entrer votre frais de livraison!",
            },
          ]}
        >
          <InputNumber
            defaultValue={2}
            min={0}
            max={10}
            step={0.01}
            formatter={(value) => `${value}€`}
            parser={(value) => value.replace("€", "")}
          />
        </Form.Item>

        <Form.Item
          name="agreement"
          valuePropName="checked"
          rules={[
            {
              validator: (_, value) =>
                value
                  ? Promise.resolve()
                  : Promise.reject("Vous devez accepter le règlement"),
            },
          ]}
          {...tailFormItemLayout}
        >
          <Checkbox>
            J'ai lu et j'accepte le <a href="/">règlement</a>
          </Checkbox>
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit">
            Register
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default RegistrationRestaurantForm;
