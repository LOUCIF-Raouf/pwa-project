import { Button, Checkbox, Form, Input, Select } from "antd";
import React, { useContext } from "react";
import { useHistory } from "react-router-dom";
import AppContext from "../../Context/AppContext";

const { Option } = Select;

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    xs: {
      span: 240,
    },
    sm: {
      span: 16,
    },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

const SignUpForm = () => {
  const { firebase } = useContext(AppContext);
  const [form] = Form.useForm();
  let history = useHistory();

  const onFinish = (values) => {
    let x = firebase
      .signUp(values.email, values.password)
      .then(async (response) => {
        let x = await firebase.getImage("no-image.png");
        return firebase.user(response.user.uid).set({
          firstname: values.firstname,
          lastname: values.lastname,
          email: values.email,
          phone: values.phone,
          address: values.address,
          type: "utilisateur",
          image: x,
        });
        history.push({ pathname: "/" });
      })
      .catch((error) => {
        switch (error.code) {
          case "auth/email-already-in-use":
            form.setFields([
              {
                name: "email",
                errors: [error.message],
              },
            ]);
            break;
          case "auth/weak-password":
            form.setFields([
              {
                name: "password",
                errors: [error.message],
              },
            ]);
            break;
          default:
            console.log("no error ", error);
            break;
        }
      });
  };

  const prefixSelector = (
    <Form.Item name="prefix" noStyle>
      <Select
        style={{
          width: 70,
        }}
      >
        <Option value="33">+33</Option>
      </Select>
    </Form.Item>
  );

  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        flexDirection: "column",
        justifyContent: "center",
        overflow: "auto",
      }}
    >
      <Form
        {...formItemLayout}
        form={form}
        name="register"
        onFinish={onFinish}
        initialValues={{
          prefix: "33",
        }}
        scrollToFirstError
      >
        <Form.Item
          name="firstname"
          label="Prénom"
          rules={[
            {
              required: true,
              message: "Veuillez saisir votre Prénom!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="lastname"
          label="Nom"
          rules={[
            {
              required: true,
              message: "Veuillez saisir votre Nom de famille!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="email"
          label="E-mail"
          rules={[
            {
              type: "email",
              message: "L'e-mail n'est pas valide!",
            },
            {
              required: true,
              message: "Veuillez saisir votre E-mail!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="password"
          label="Mot de passe"
          rules={[
            {
              required: true,
              message: "Veuillez saisir votre mot de passe!",
            },
          ]}
          hasFeedback
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          name="confirm"
          label="Confirmer le mot de passe"
          dependencies={["password"]}
          hasFeedback
          rules={[
            {
              required: true,
              message: "Veuillez confirmer votre mot de passe!",
            },
            ({ getFieldValue }) => ({
              validator(rule, value) {
                if (!value || getFieldValue("password") === value) {
                  return Promise.resolve();
                }

                return Promise.reject(
                  "Les deux mots de passe que vous avez saisis ne correspondent pas!"
                );
              },
            }),
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          name="phone"
          label="Téléphone"
          rules={[
            {
              required: true,
              message: "Veuillez entrer votre numéro de téléphone!",
            },
          ]}
        >
          <Input
            addonBefore={prefixSelector}
            style={{
              width: "100%",
            }}
          />
        </Form.Item>

        <Form.Item
          name="address"
          label="Adresse"
          rules={[
            {
              required: true,
              message: "Veuillez entrer votre adresse!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="agreement"
          valuePropName="checked"
          rules={[
            {
              validator: (_, value) =>
                value
                  ? Promise.resolve()
                  : Promise.reject("Vous devez accepter le règlement"),
            },
          ]}
          {...tailFormItemLayout}
        >
          <Checkbox>
            J'ai lu et j'accepte le <a href="/">règlement</a>
          </Checkbox>
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit">
            Register
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default SignUpForm;
