import { SearchOutlined } from "@ant-design/icons";
import { Button, Card, Col, Tooltip } from "antd";
import React from "react";
import { Link } from "react-router-dom";
import Home from "../Home/Home";
import Image from "../../Components/Image/Image"

const { Meta } = Card;

export const Restaurant = ({ restaurant }) => {

  const image ={
    alt:'restaurant',
    src:`https://source.unsplash.com/300x300/?${restaurant.logo}`
  }
  return (
    <div>
      <Link to={`/app/restaurants/${restaurant.id || restaurant.objectID}`}>
        <Col xs={24} sm={24} md={24} lg={24} xl={5}>
          <br />
          <Card
            style={{ width: 300, height: 450 }}
            cover={
              <Image image={image}/>
            }
          >
            <Meta title="Informations" />
            <div>{restaurant.name}</div>
            <div>{restaurant.address}</div>
            <Tooltip title="(☞ﾟヮﾟ)☞">
              <Button
                style={{ left: "90%" }}
                type="primary"
                shape="circle"
                icon={<SearchOutlined />}
              />
            </Tooltip>
          </Card>
        </Col>
      </Link>
    </div>
  );
};

const Restaurants = () => {
  return <Home />;
};

export default Restaurants;
