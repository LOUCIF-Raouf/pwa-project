import { HeartOutlined } from "@ant-design/icons";
import { Button, Card, Col, Row, Space, Table } from "antd";
import React, { useContext, useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import Commentaires from "../../Components/Commentaire/Commentaires";
import Image from "../../Components/Image/Image";
import { Loading } from "../../Components/Loading/Loading";
import AppContext from "../../Context/AppContext";

const { Meta } = Card;

const Restaurant = ({ restaurant }) => {
  return (
    <div>
      <div>{restaurant.name}</div>
      <div>{restaurant.address}</div>
    </div>
  );
};

const ShowRestaurants = () => {
  const { id } = useParams();
  const [isFavorite, setIsFavorite] = useState();
  const [produits, setProduits] = useState([]);
  const [loading, setLoading] = useState(false);
  const [quantity, setQuantity] = useState();
  const [restaurant, setRestaurant] = useState({});
  const [panier, setPanier] = useState();

  const image = {
    alt: "restaurant",
    src: `https://source.unsplash.com/300x300/?${restaurant.logo}`,
    width: 300,
    height: 300,
  };

  let history = useHistory();
  const {
    isDisconnected,
    restaurants,
    rests,
    firebase,
    getLocalUser,
    setLocalUser,
    localUser,
  } = useContext(AppContext);

  useEffect(() => {
    restaurants();
    console.log("ok");
    getLocalUser();
  }, []);

  useEffect(() => {
    setLoading(true);

    if (isDisconnected) {
      let rest = rests.filter((r) => r.id === id).pop();
      console.log(rest);
      let products = [];
      if (rest.products) {
        products = Object.entries(rest.products).map((p, index) => {
          return { key: index, name: p[0], price: p[1], quantity: 0 };
        });
      }
      setRestaurant(rest);
      setProduits(products);
      setLoading(false);
    } else {
      firebase
        .getUser(id)
        .then((response) => {
          setRestaurant(response);
          let products = [];
          if (response.products) {
            console.log(Object.entries(response.products));
            products = Object.entries(response.products).map((p, index) => {
              return { key: index, name: p[0], price: p[1], quantity: 0 };
            });
          }
          setProduits(products);
          setLoading(false);
        })
        .catch((error) => console.log(error));
    }
    /*
    firebase.getUser(firebase.currentUserUid()).then((response) => {
      if (response.favorites.includes(id)) {
        setIsFavorite(<HeartFilled />);
      } else {
        setIsFavorite(<HeartOutlined />);
      }
    });*/
  }, []);

  const columns = [
    {
      title: "Produit",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Price",
      dataIndex: "price",
      key: "price",
    },
    {
      title: "Quantity",
      dataIndex: "quantity",
      key: "quantity",
    },
    {
      title: "Action",
      key: "action",
      render: (text, record) => (
        <Space size="middle">
          <Button
            type="primary"
            disabled={quantity === 0 || isDisconnected}
            onClick={() => setQuantity((record.quantity = record.quantity - 1))}
          >
            -
          </Button>

          <Button
            type="primary"
            onClick={() => {
              console.log(record);
              setQuantity((record.quantity = record.quantity + 1));
            }}
            disabled={isDisconnected}
          >
            +
          </Button>
        </Space>
      ),
    },
  ];

  return (
    <div>
      <Row type="flex">
        <Col xs={24} sm={24} md={24} lg={24} xl={5}>
          <br />
          <Card
            style={{ width: 300, height: 450 }}
            cover={<Image image={image} />}
          >
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <Meta title="Informations" />

              <Button
                style={{ left: "90%" }}
                shape="circle"
                title="Favoris"
                style={{
                  color: isDisconnected
                    ? localUser
                      ? localUser.favorites.includes(id)
                        ? "#096dd9"
                        : "#d9d9d9"
                      : "#d9d9d9"
                    : isFavorite
                    ? "#096dd9"
                    : "#d9d9d9",
                  border: isDisconnected
                    ? localUser
                      ? localUser.favorites.includes(id)
                        ? "1px solid #096dd9"
                        : "1px solid #d9d9d9"
                      : "1px solid #d9d9d9"
                    : isFavorite
                    ? "1px solid #096dd9"
                    : "1px solid #d9d9d9",
                }}
                onClick={() => {
                  if (isDisconnected) {
                    localUser.sync = "update";

                    if (localUser.favorites.includes(id)) {
                      localUser.favorites = localUser.favorites.filter(
                        (u) => u !== id
                      );
                      setLocalUser(localUser);
                    } else {
                      localUser.favorites.push(id);
                      setLocalUser(localUser);
                    }
                  } else {
                    firebase.setFavorite(id);
                    setIsFavorite(true);
                  }

                  console.log(localUser);
                }}
                icon={<HeartOutlined />}
              />
            </div>
            <Restaurant restaurant={restaurant} />
          </Card>
        </Col>
        <Col xs={24} sm={24} md={24} lg={24} xl={19}>
          <br />
          {loading ? (
            <Loading />
          ) : (
            <Table columns={columns} dataSource={produits} />
          )}
          <div>
            <Button
              type="primary"
              block
              onClick={async () => {
                const commande = produits.filter((p) => p.quantity > 0);
                setLoading(true);

                const userId = firebase.currentUserUid();
                let orderId = await firebase.addOrder(
                  userId,
                  restaurant.id,
                  commande
                );
                history.push({
                  pathname: `/app/panier/${orderId}`,
                  state: { restaurant: restaurant },
                });
                setLoading(false);
              }}
              disabled={isDisconnected}
            >
              Voir la commande
            </Button>
          </div>
        </Col>
        <Col xs={24} sm={24} md={24} lg={24} xl={24}>
          <Commentaires dataRestaurant={id} />
        </Col>
      </Row>
    </div>
  );
};

export default ShowRestaurants;
