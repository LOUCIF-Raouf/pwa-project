import { Button, notification, Space, Table } from "antd";
import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { withFirebase } from "../../Components/Firebase";
import Loading from "../../Components/Loading/Loading";

const openNotificationWithIcon = (type, placement) => {
  notification[type]({
    message: "Commande envoyé",
    description:
      "Votre commande a été envoyé a votre restaurant! Vous serez livré prochainement",
    placement,
    duration: 0,
  });
};

const PanierFirebase = ({ firebase }) => {
  const { id } = useParams();
  const [produitsPanier, setProduitsPanier] = useState([]);
  const [loading, setLoading] = useState(false);
  let history = useHistory();

  const columns = [
    {
      title: "Produit",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Price",
      dataIndex: "price",
      key: "price",
    },
    {
      title: "Quantity",
      dataIndex: "quantity",
      key: "quantity",
    },
    {
      title: "Action",
      key: "action",
      render: (text, record) => (
        <Space size="middle">
          <Button
            type="primary"
            disabled={record.quantity === 0}
            onClick={() => {
              updateOrder(record, "decrement");
            }}
          >
            -
          </Button>

          <Button
            type="primary"
            onClick={() => {
              updateOrder(record, "increment");
            }}
          >
            +
          </Button>
        </Space>
      ),
    },
  ];
  const columnsPréparation = [
    {
      title: "Produit",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Prix",
      dataIndex: "price",
      key: "price",
    },
    {
      title: "Quantité",
      dataIndex: "quantity",
      key: "quantity",
    },
  ];

  useEffect(() => {
    setLoading(true);

    firebase
      .getOrder(id)
      .then((response) => {
        setProduitsPanier(response);
        setLoading(false);
      })
      .catch((error) => console.log(error));
  }, []);

  const updateOrder = (record, type) => {
    setLoading(true);
    console.log(produitsPanier.commande);
    let order = {
      ...produitsPanier,
      commande: produitsPanier.commande.map((c) => {
        if (c.key === record.key)
          return {
            ...c,
            quantity:
              type === "increment" ? record.quantity + 1 : record.quantity - 1,
          };
        else return { ...c };
      }),
    };

    firebase.setOrder(id, order);
    setProduitsPanier(order);

    setLoading(false);
  };

  const sendOrder = () => {
    setLoading(true);
    firebase
      .sendOrder(id)
      .then((response) => {
        setProduitsPanier(response);
        setLoading(false);
      })
      .catch((error) => console.log(error));
  };

  console.log(history);

  return (
    <>
      {loading && <Loading />}
      {!loading && produitsPanier && (
        <>
          {produitsPanier.status === "En attente de validation" ? (
            <>
              <Table columns={columns} dataSource={produitsPanier.commande} />
              <Button
                onClick={() => {
                  openNotificationWithIcon("success", "topRight");
                  sendOrder();
                }}
              >
                Confirmer la commande
              </Button>
            </>
          ) : (
            <>
              <Table
                columns={columnsPréparation}
                dataSource={produitsPanier.commande}
              />
            </>
          )}
        </>
      )}
    </>
  );
};

const Panier = withFirebase(PanierFirebase);

export default Panier;
