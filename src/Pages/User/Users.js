import { SearchOutlined } from "@ant-design/icons";
import { Button, Card, Col, Row, Tooltip } from "antd";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { withFirebase } from "../../Components/Firebase";
import { Loading } from "../../Components/Loading/Loading";
import Image from "../../Components/Image/Image"

const { Meta } = Card;

const User = ({ user }) => {
  const image ={
    alt:'user',
    src: user.image,
    width: 300, 
    height: 300
  }

  return (
    <div>
      <Link to={`/app/user/${user.id}`}>
        <Col xs={24} sm={24} md={24} lg={24} xl={5}>
          <br />
          <Card
            style={{ width: 300, height: 450 }}
            cover={
              <Image image={image}/>
            }
          >
            <Meta title="Informations" />
            <div>
              {user.firstname} {user.lastname}
            </div>
            <Tooltip title="(☞ﾟヮﾟ)☞">
              <Button
                style={{ left: "90%" }}
                type="primary"
                shape="circle"
                icon={<SearchOutlined />}
              />
            </Tooltip>
          </Card>
        </Col>
      </Link>
    </div>
  );
};

const UsersFirebase = ({ firebase }) => {
  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);

    firebase
      .getUsers()
      .then((response) => {
        setUsers(response);
        setLoading(false);
      })
      .catch((error) => console.log(error));
  }, []);

  return (
    <div>
      <Row type="flex" gutter={50}>
        {loading && <Loading />}
        {!loading && users.map((user) => <User user={user} firebase={firebase} />)}
      </Row>
    </div>
  );
};

const Users = withFirebase(UsersFirebase);

export default Users;
