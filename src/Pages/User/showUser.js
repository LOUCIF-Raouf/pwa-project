import { MailOutlined, PhoneFilled, PushpinFilled } from "@ant-design/icons";
import { Button, Card, Col, Row, Table } from "antd";
import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import Chat from "../../Components/Chat/Chat";
import { withFirebase } from "../../Components/Firebase";
import Image from "../../Components/Image/Image";
import { Loading } from "../../Components/Loading/Loading";

const { Meta } = Card;

const UserInfo = ({ user }) => {
  return (
    <div>
      <div>
        {user.firstname} {user.lastname}
      </div>
      <div>
        <MailOutlined /> {user.email}
      </div>
      <div>
        <PushpinFilled /> {user.address}
      </div>
      <div>
        <PhoneFilled /> {user.phone}
      </div>
    </div>
  );
};

const ShowUserFirebase = ({ firebase }) => {
  let history = useHistory();
  const { id } = useParams();
  const [user, setUser] = useState([]);
  const [loading, setLoading] = useState(false);
  const reducer = (accumulator, currentValue) => accumulator + currentValue;
  const [dataSource, setDataSource] = useState([]);
  console.log(user);
  const columns = [
    {
      title: "Date de la commande",
      dataIndex: "dateCommande",
      key: "dateCommande",
    },
    {
      title: "Status de la commande",
      dataIndex: "status",
      key: "status",
    },
    {
      title: "Prix",
      dataIndex: "prix",
      key: "prix",
    },
    {
      title: "Commande",
      dataIndex: "commande",
      key: "commande",
    },
  ];
  const image = {
    alt: "user",
    src: user ? user.image : "",
    width: 300,
    height: 300,
    onClick: changeImage,
  };

  useEffect(() => {
    setLoading(true);

    firebase
      .getUser(id)
      .then((response) => {
        setUser(response);
        //console.log(Object.entries(response.products));
        setLoading(false);
      })
      .catch((error) => console.log(error));

    firebase
      .getOrderByUser(id)
      .then((response) => {
        console.log(response);
        var data = response.map((order) => {
          var prix = order.commande.map(
            (produit) => produit.price * produit.quantity
          );
          console.log(prix);
          return {
            dateCommande: order.date_commande,
            status: order.status,
            prix: prix ? (prix.length > 0 ? prix.reduce(reducer) + "€" : 0) : 0,
            commande: (
              <Button
                type="primary"
                block
                onClick={async () => {
                  history.push({ pathname: `/app/panier/${order.id}` });
                }}
              >
                Voir la commande
              </Button>
            ),
          };
        });
        setDataSource(data);
        //console.log(Object.entries(response.products));
        setLoading(false);
      })
      .catch((error) => console.log(error));
  }, []);

  return (
    <div>
      <Row type="flex">
        <Col xs={24} sm={24} md={24} lg={24} xl={5}>
          <br />
          <Card
            style={{ width: 300, height: 475 }}
            cover={<Image image={image} />}
          >
            <Meta title="Informations" />
            <UserInfo user={user} />
            <Button type="primary" htmlType="submit" onClick={logout}>
              Logout
            </Button>
          </Card>
        </Col>
        <Col xs={24} sm={24} md={24} lg={24} xl={19}>
          <br />
          {loading ? (
            <Loading />
          ) : (
            <Table columns={columns} dataSource={dataSource} />
          )}
        </Col>
      </Row>
      <Chat />
    </div>
  );

  async function logout() {
    await firebase.signOut();
    history.push("/auth/login");
  }

  async function changeImage() {
    history.push("/app/user/" + firebase.currentUserUid() + "/changeImage");
  }
};

const ShowUser = withFirebase(ShowUserFirebase);

export default ShowUser;
