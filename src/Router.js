import React, { Suspense } from "react";
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from "react-router-dom";
//import Chat from "./Components/Chat/Chat";
//import Lost from "./Components/Lost/Lost";
//import Sign from "./Components/SignForm/SignForm";
//import Uploader from "./Components/Uploader/Uploader";
import { AppProviderFirebase } from "./Context/AppContext";
//import Home from "./Pages/Home/Home.js";
import Layout from "./Pages/Layout/Layout";
//import Login from "./Pages/Login/Login";
//import RegistrationRestaurantForm from "./Pages/Register/RegisterRestaurant";
//import RegistrationUserForm from "./Pages/Register/RegisterUser";
//import Restaurants from "./Pages/Restaurants/Restaurants";
//import ShowRestaurants from "./Pages/Restaurants/ShowRestaurants";
//import Panier from "./Pages/User/Panier";
//import ShowUser from "./Pages/User/showUser";
//import Users from "./Pages/User/Users";

const Chat = React.lazy(() => import("./Components/Chat/Chat"));
const Lost = React.lazy(() => import("./Components/Lost/Lost"));
const Sign = React.lazy(() => import("./Components/SignForm/SignForm"));
const Home = React.lazy(() => import("./Pages/Home/Home.js"));
const Uploader = React.lazy(() => import("./Components/Uploader/Uploader"));
const Login = React.lazy(() => import("./Pages/Login/Login"));
const RegistrationRestaurantForm = React.lazy(() =>
  import("./Pages/Register/RegisterRestaurant")
);
const RegistrationUserForm = React.lazy(() =>
  import("./Pages/Register/RegisterUser")
);
const Restaurants = React.lazy(() => import("./Pages/Restaurants/Restaurants"));
const ShowRestaurants = React.lazy(() =>
  import("./Pages/Restaurants/ShowRestaurants")
);
const Panier = React.lazy(() => import("./Pages/User/Panier"));
const ShowUser = React.lazy(() => import("./Pages/User/showUser"));
const Users = React.lazy(() => import("./Pages/User/Users"));

const App = () => {
  return (
    <Router>
      <AppProviderFirebase>
        <Suspense fallback={<div>Chargement...</div>}>
          <Switch>
            <Route exact path="/">
              <Redirect to="/auth/login" />
            </Route>
            <Route path="/app">
              <Layout>
                <Route exact path="/app/user/:id">
                  <ShowUser />
                </Route>
                <Route exact path="/app/user/:id/changeImage">
                  <Uploader />
                </Route>
                <Route exact path="/app/list/restaurants">
                  <Restaurants />
                </Route>
                <Route path="/app/list/utilisateurs">
                  <Users />
                </Route>
                <Route path="/app/restaurants/:id">
                  <ShowRestaurants />
                </Route>
                <Route path="/app/panier/:id">
                  <Panier />
                </Route>
                <Route exact path="/app/chat">
                  <Chat />
                </Route>
                <Route exact path="/app">
                  <Home />
                </Route>
              </Layout>
            </Route>
            <Route path="/auth">
              <Sign>
                <Route path="/auth/login">
                  <Login />
                </Route>
                <Route path="/auth/restaurant">
                  <RegistrationRestaurantForm />
                </Route>
                <Route path="/auth/register">
                  <RegistrationUserForm />
                </Route>
              </Sign>
            </Route>
            <Route component={Lost} path="*" />
          </Switch>
        </Suspense>
      </AppProviderFirebase>
    </Router>
  );
};

export default App;
